# Load balancer

## Defination
Load balancer is a technique where you can spread workload equaly in servers. It is helpful for optimize your computer network efficiency.

## Uses
* Load balancer used for backup purpose. If one server goes down then another backup server start working immediately.
* Load balancer is needs for smooth performance.

## Types
### Two types of Load Balancer present they are :-
1. **Software Load Balancer :-** Software load balancer are often installed on the servers and consumes the processor and memory of the servers.


1. **Hardware Load Balancer :-** Hard ware load balancers are specialized hardware deployed in-between server and the client.

## Image
![Load Balancer](https://avinetworks.com/wp-content/uploads/2017/04/load-balancer-with-software-load-balancer-hardware-load-balancer-diagram.png)
## Top five Load Balancer

* F5 Load Balancer BIG IP Platform.
* A10 Application Delivery & Load Balancer.
* Citrix ADC (formerly NetScaler ADC)
* Avi Vantage Software Load Balancer.
* Radware's Alteon Application Delivery Controller.




# Vertical/Horizontal scaling
**Vertical and Horizontal Scaling needs for expand the system performance upto expected level.**

## Defination
**Horizontal Scaling** means add more resources like servers for getting high performance. It extends size in Horizonally. It is difficult to implement. It is costlier then Vertical Scaling.

**Vertical Scaling** means add more resources like RAM,CPU for getting high performance. It extends size in Vertically. It is easy to implement. It is cheaper then Horizontal Scaling.


## Image
![Image For Horizontal And Vertical Scaling](https://miro.medium.com/max/502/0*1e82F85tbxYp4pBw.png)



# Conclusion
**Both Horizontal and Vertical Scaling has some pros and also cons**

1. Load Balancing required in Horizontal Scaling but in Vertical Scaling not required.
1. Horizontal Scaling is RESILIENT but Vertical Scaling is single point of failure.
1. Horizontal Scaling processes slower then Vertical Scaling processes.
1. In Horizontal Scaling DATA INCONSISTENCY present but in Vertical Scaling data is CONSISTENT.
1. In Horizontal Scaling no limitation present but in Vertical Scaling hardware limitation presents.


# References

1. [Load Balancing](https://searchnetworking.techtarget.com/definition/load-balancing)

1. [Google Search Get small small topic on Load Balancing](https://www.google.com/search?sxsrf=ALeKk03RV1fTe508poUJtJFxkX77rnxYHA%3A1596871322334&source=hp&ei=mlIuX7b5EeTjz7sP4_i34Ao&q=load+balancer&oq=load+balancer&gs_lcp=CgZwc3ktYWIQAzIECCMQJzIICAAQsQMQkQIyBwgAEBQQhwIyAggAMgIIADICCAAyAggAMgIIADICCAAyAggAOgUIABCRAjoFCC4QsQM6CAguELEDEIMBOgUIABCxAzoICAAQsQMQgwE6CggAELEDEBQQhwI6BAgAEApQ2wZY0R9g1iJoAHAAeAGAAc8CiAHJEZIBCDAuMTEuMC4ymAEAoAEBqgEHZ3dzLXdpeg&sclient=psy-ab&ved=0ahUKEwi2rqufiYvrAhXk8XMBHWP8DawQ4dUDCAc&uact=5)

1. [Horizontal And Vertical Scaling From Greekforgreek](https://www.geeksforgeeks.org/horizontal-and-vertical-scaling-in-databases/)

1. [Horizontal And Vertical Scaling From medium.com](https://medium.com/@abhinavkorpal/scaling-horizontally-and-vertically-for-databases-a2aef778610c)

1. [Youtube Video](https://www.youtube.com/watch?v=xpDnVSmNFX0)